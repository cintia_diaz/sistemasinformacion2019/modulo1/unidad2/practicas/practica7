﻿USE world;

/*
  CONSULTA 1: 
  Listar todos los lenguajes oficiales de España
*/
 
CREATE OR REPLACE VIEW vista1 AS
  SELECT 
  c1.Language 
FROM country c 
JOIN countrylanguage c1 
ON c.Code = c1.CountryCode 
WHERE c.LocalName='España'
AND c1.IsOfficial='T';


/*
  CONSULTA 2:
  Listar todos los lenguajes de España
*/

CREATE OR REPLACE VIEW vista2 AS
  SELECT 
    c1.Language 
  FROM country c 
  JOIN countrylanguage c1 
  ON c.Code = c1.CountryCode 
  WHERE c.LocalName='España';

/*
  CONSULTA 3: 
  Indicar la poblacion media por continente y región (ordenándolo por ambos campos)
*/

CREATE OR REPLACE VIEW vista3 AS
  SELECT 
  c.Continent, c.Region, AVG(c.Population) poblacionMedia 
FROM country c 
GROUP BY c.Continent, c.Region;



/*
  CONSULTA 4: 
  Indicar el nombre de todas las ciudades del continente cuya poblacion media es la mayor
*/

-- c1: poblacion media por continente

    CREATE OR REPLACE VIEW vista4a AS
      SELECT 
        Continent, AVG(Population) poblMedia 
      FROM country 
      GROUP BY Continent;

     
-- c2: calculo el máximo de c1
    CREATE OR REPLACE VIEW vista4b AS
      SELECT 
        MAX(poblMedia) poblMayor
      FROM vista4a;

-- c3: continente con mayor poblacion media
  CREATE OR REPLACE VIEW vista4c AS
    SELECT 
      Continent 
    FROM vista4a
    JOIN vista4b
    ON poblMedia=poblMayor;

 -- final: ciudades del continente con poblacion mmedia mayor
  CREATE OR REPLACE VIEW vista4 AS
    SELECT 
      city.Name
    FROM vista4c
    JOIN country 
    USING (Continent)
    JOIN city 
    ON country.Code = city.CountryCode;

  

  /*
    CONSULTA 5: Indicar la población media de las ciudades del continente cuyos países tengan
                la esperanza de vida media mayor
  */


    -- c1: esperanza de vida media por continente

    CREATE OR REPLACE VIEW vista5a AS
      SELECT 
        Continent, AVG(LifeExpectancy) espvidaMedia 
      FROM country 
      GROUP BY Continent;

    -- c2: calculo el máximo de c1
    CREATE OR REPLACE VIEW vista5b AS
      SELECT 
        MAX(espvidaMedia) espvidaMayor
      FROM vista5a;
    

    -- Continente con esperanza de vida mayor
      CREATE OR REPLACE VIEW vista5c AS
        SELECT 
          Continent 
        FROM vista5a
        JOIN vista5b
        ON espvidaMedia=espvidaMayor;
      

    -- Código de los paises del continente
      CREATE OR REPLACE VIEW vista5d AS
        SELECT 
          DISTINCT Code 
        FROM vista5c
        JOIN country USING(Continent);

    -- poblacion media de las ciudades del contiente
      CREATE OR REPLACE VIEW vista5 AS
       SELECT 
        AVG(Population) poblacionMedia
       FROM vista5d 
       JOIN city 
       ON vista5d.code=CountryCode;

   
    /*
      CONSULTA 6: 
      Indicar la población y el nombre de las capitales de cada uno de los países.
      Además, debemos indicar el país del cual es capital y el nombre del continente al que pertenece.
   */

      CREATE OR REPLACE VIEW vista6 AS
        SELECT city.Population POBLACION, city.Name CIUDAD, country.Name PAIS, Continent CONTINENTE
        FROM country 
        JOIN city 
        ON country.Code = city.CountryCode 
        AND Capital=ID;


  /*
    CONSULTA 7:
    Indicar el lenguaje en cada pais cuyo porcentaje sea el mayor (percentage)
  */

    -- c1: porcentaje máximo de lenguaje por país
    CREATE OR REPLACE VIEW vista7a AS 
      SELECT 
        c.CountryCode, MAX(c.Percentage) maximo 
      FROM countrylanguage c 
      GROUP BY c.CountryCode;
  
    -- c2: Código de país y su lenguaje con mayor porcentaje
    CREATE OR REPLACE VIEW vista7b AS
      SELECT 
        c.CountryCode, c.Language 
      FROM countrylanguage c 
      JOIN vista7a 
      ON c.Percentage=maximo AND c.CountryCode=vista7a.CountryCode;

    -- final: Nombre del país y el lenguaje con mayor porcentaje
    CREATE OR REPLACE VIEW vista7 AS
      SELECT c.Name, v.Language 
        FROM vista7b v
        JOIN country c ON v.CountryCode=c.Code;
 
    /*
      CONSULTA 8:
      Indicar los continentes que mayor número de lenguas se hablen
    */

      -- c1: cuento el número de lenguas por continente
      CREATE OR REPLACE VIEW vista8a AS
        SELECT 
          c.Continent, COUNT(*) numeroLenguas
        FROM country c 
        JOIN countrylanguage c1 
        ON c.Code = c1.CountryCode 
        GROUP BY c.Continent;

      -- c2: máximo del número de lenguas
      CREATE OR REPLACE VIEW vista8b AS
        SELECT
          MAX(numeroLenguas) maximo 
        FROM vista8a v;
      SELECT * FROM vista8b;

      -- final: calculo los continente con mayor número de lenguas
      CREATE OR REPLACE VIEW vista8 AS
        SELECT 
          v.Continent 
        FROM vista8a v
        JOIN vista8b v1
        ON numeroLenguas=maximo;

    

    /*
      CONSULTA 9:
      Indicar los países que menor número de lenguas se hablen
    */

      -- c1: Calculo el número de lenguas por país
      CREATE OR REPLACE VIEW vista9a AS
        SELECT
          c.CountryCode, COUNT(*) numeroLenguas 
        FROM countrylanguage c 
        GROUP BY c.CountryCode;
    
      -- c2: Calculo el mínimo número de lenguas que se hablan
      CREATE OR REPLACE VIEW vista9b AS
        SELECT 
          MIN(numeroLenguas) minimo 
        FROM vista9a v;

      -- final: nombre de los países con menor número de lenguas
      CREATE OR REPLACE VIEW vista9 AS
        SELECT 
          name 
        FROM vista9a
        JOIN vista9b 
        ON numeroLenguas=minimo
        JOIN country c 
        ON countryCode=c.Code;


    /* 
      CONSULTA 10: Indicar los países que tengan el mismo número de lenguas que España
    */

      -- c1: Calculo el número de lenguas que tiene España
      CREATE OR REPLACE VIEW vista10a AS
        SELECT 
          COUNT(*) 
        FROM countrylanguage 
        JOIN country 
        ON CountryCode=country.Code AND Name='Spain';

      -- c2: Código de países con el mismo número de lenguas que tiene España
      CREATE OR REPLACE VIEW vista10b AS
        SELECT 
          CountryCode 
        FROM countrylanguage
        GROUP BY CountryCode 
        HAVING COUNT(*)=(SELECT * FROM vista10a);

      -- final: Nombre de los países con el mismo número de lenguas de España
      CREATE OR REPLACE VIEW vista10 AS
       SELECT 
        Name 
       FROM vista10b
       JOIN country
       ON CountryCode=Code;

      